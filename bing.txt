Feature: Search functionality on bing.com

  Scenario: Search for "Prowly Media Monitoring" content with default "All categories" filter
    Given I am on the bing.com website
    When I enter the phrase "Prowly Media Monitoring" in the search bar
    And I filter the search 
    Then I should see relevant information related to "Prowly Media Monitoring"
    
  Scenario: Search for "Prowly Media Monitoring" with Chat content filter
    Given I am on the bing.com website
    When I enter the phrase "Prowly Media Monitoring" in the search bar
    And I filter the search results by content category "Chat"
    Then the drop-down "Chat" powered with "copilot" feature show relevant information related to "Prowly Media Monitoring"
    
  Scenario: Search for "Prowly Media Monitoring" with images content filter
    Given I am on the bing.com website
    When I enter the phrase "Prowly Media Monitoring" in the search bar
    And I filter the search results by content category "Images"
    Then I should see relevant image results related to "Prowly Media Monitoring"

  Scenario: Search for "Prowly Media Monitoring" with video content filter
    Given I am on the bing.com website
    When I enter the phrase "Prowly Media Monitoring" in the search bar
    And I filter the search results by content category "Videos"
    Then I should see relevant video results related to "Prowly Media Monitoring"
    
  Scenario: Search for "Prowly Media Monitoring" with news content filter
    Given I am on the bing.com website
    When I enter the phrase "Prowly Media Monitoring" in the search bar
    And I filter the search results by content category "News"
    Then I should see relevant news results related to "Prowly Media Monitoring"
    
  Scenario: Search for "Prowly Media Monitoring" with map content filter
    Given I am on the bing.com website
    When I enter the phrase "Prowly Media Monitoring" in the search bar
    And I filter the search results by content category "Maps"
    Then I shouldn't see results related to "Prowly Media Monitoring"